const {useServer} = require('@domino/domino-db');
const path = require('path');
const fs = require('fs');
const http = require('http');


const databaseConfig = {
	 filePath: 'Binay/readdata/IMS_Data.nsf',
};



const readfile = fileName =>{
	try {
	    return fs.readFileSync (path.resolve (fileName));
  	} catch (error) {
	    console.log ("Error reading" + fileName);
	    return undefined;
  }
};


const rootCertificate = readfile('ca.crt');
const clientCertificate = readfile('NodeJS.crt');
const clientKey = readfile('NodeJS.key');




// proton config
const serverConfig = {
    "hostName": "dagobah.primaxis.com",
    "connection": {
        "port": 3002,
        secure: true,
    },
      credentials: {
      rootCertificate,
      clientCertificate,
      clientKey,
  },
};





useServer(serverConfig)
	.then(async server =>{
		
		database = await server.useDatabase(databaseConfig);

		const document = await database.useDocument({unid: '40DBCD5E8CC7B23ECA25846300811960',});

		try{
			const mydoc = await document.read();
			console.log(await document.getUnid());
			 // const document = await database.bulkReadAttachmentStreamByUnid({
				// 			    unids: [
				// 			      '40DBCD5E8CC7B23ECA25846300811960',
				// 			      '5826CA6FCD2B0D57CA258463008106B3',
				// 			    ],});

			 // console.log("Document");
			 // console.log(await document.read());


		}
		catch(error){
			console.log(error);
		}


		
	});

