const { useServer } = require('@domino/domino-db');
const path = require('path');
const fs = require('fs');
const http = require('http');
const express = require('express');
//import { filePathToPath} from 'url';

let app = express();

let crntarray = ["one"];


const databaseConfig = {
  filePath: 'Binay/readdata/IMS_Data.nsf', // The database file name
};



const BOOK_fieldName =["UpdatedBy",
"BOOK_Agency_FlowComp",
"BOOK_Agency_ID",
"BOOK_Agency_Interpreter",
"BOOK_Agency_Interpreter_Gender",
"BOOK_Agency_Interpreter_Level",
"BOOK_Agency_Interpreter_Number",
"BOOK_Agency_Last_Updated",
"BOOK_Agency_SentBy",
"BOOK_Agency_SentDate",
"BOOK_Agency_URL",
"BOOK_Agency_ApptDuration",
"BOOK_ApptDuration",
"BOOK_ApptDuration_Actual",
"BOOK_ApptFinishTime_Actual",
"BOOK_ApptStartDate_Text",
"BOOK_ApptStartTime_Actual",
"BOOK_ApptStart_History",
"BOOK_ApptStart_End",
"BOOK_Appt_Start",
"BOOK_BookingType",
"BOOK_CancellationNotice",
"BOOK_CancellationReason",
"BOOK_Clinic",
"BOOK_Clinician",
"BOOK_ClinicianContact",
"BOOK_Comments",
"BOOK_CommentsInterpreter",
"BOOK_Completed",
"BOOK_CostCentre",
"BOOK_DatePassed",
"BOOK_ExportPAddress",
"BOOK_ExportPatientAddress",
"BOOK_ExportPPostCode",
"BOOK_ExportPState",
"BOOK_ExportPSuburb",
"BOOK_Extra",
"BOOK_Interpreter_DocKey",
"BOOK_ImportString",
"BOOK_Interpreter_DocKey",
"BOOK_Interpreter_FullName",
"BOOK_Interpreter_Type",
"BOOK_IsHome",
"BOOK_Language",
"BOOK_Language_Code",
"BOOK_Location",
"BOOK_Log",
"BOOK_NC_Reason",
"BOOK_NC_Reason_InterpreterFTA",
"BOOK_NC_Reason_InterpreterNA",
"BOOK_NC_Reason_InterpreterNR",
"BOOK_NC_Reason_Other",
"BOOK_PatientAddress",
"BOOK_PatientDOB_Text",
"BOOK_PatientFirstName",
"BOOK_PatientGender",
"BOOK_PatientGenderPreference",
"BOOK_PatientLastName",
"BOOK_PatientPostCode",
"BOOK_PatientState",
"BOOK_PatientSuburb",
"BOOK_PatientType",
"BOOK_PatientUR",
"BOOK_Patient_DOB_Text",
"BOOK_Reallocated",
"BOOK_RequestedBy",
"BOOK_RequestedByContact",
"BOOK_RequestedBy_DocKey",
"BOOK_RescheduledDocKey",
"BOOK_Site",
"BOOK_SiteName",
"BOOK_Source_Display",
"BOOK_Status",
"BOOK_Status_Display",
"BOOK_WaitingDuration",
"CBN_Suggestion_Key",
"CreatedBy",
"CreatedDate",
"DocKey",
"DocKey_CreatedBy",
"form",
"JSON_AgencySentBy",
"JSON_AgencySentDate",
"JSON_AgencySentTime",
"JSON_Appt_EndDate",
"JSON_Appt_EndTime",
"JSON_Appt_StartDate",
"JSON_Appt_StartTime",
"JSON_Language",
"JSON_PFirstName",
"JSON_PGender",
"JSON_PLastName",
"ModifiedBy",
"ModifiedDate",
"UNID",
"View_Appt_EndTime",
"View_Appt_StartDate"]


const readfile = fileName =>{
	try {
    return fs.readFileSync (path.resolve (fileName));
  	} catch (error) {
    console.log ("Error reading" + fileName);
    return undefined;
  }
};

const rootCertificate = readfile('certificates/ca.crt');
const clientCertificate = readfile('certificates/NodeJs.crt');
const clientKey = readfile('certificates/NodeJS.key');


// proton config
const serverConfig = {
    "hostName": "dagobah.primaxis.com",
    "connection": {
        "port": 3002,
        secure: true,
    },
      credentials: {
      rootCertificate,
      clientCertificate,
      clientKey,
  },
};


useServer(serverConfig)
    .then(async server =>{
      const database = await server.useDatabase(databaseConfig);
      /**
       *  Read Document from the database  
       */
       database.bulkReadDocuments({                            
                            query: "Form = 'BOOK'",
                            start:0,                            //specify the starting number of the document
                            count:10,                           //specify the the of document to read 
                            itemNames:BOOK_fieldName,           //the array of the names to get the data from the form 
                          }).then(response=>{
                            //console.log(response);              //json object as a response after reading from the database 
                              for(var i=0;i<10;i++){                             
                                  //console.log(BOOK_fieldName[i]+": "+response.documents[i].BOOK_CancellationReason);
                                  console.log(crntarray);
                                  console.log('type of field '+typeof(BOOK_fieldName[i]));
                                  
                                  crntarray.push(BOOK_fieldName[i]);
                              }
                          }).catch(error=>console.log("Error",error));

        /**
         * Create Document in the Database
         */

        const unidreturn = await database.createDocument({
              document: {
                  Form:'BOOK',                                  //specify the Form to create the document
                  DocKey: 'BINA-1234',                          //The fields to create 
                  BOOK_PatientFirstName:'NodeJs',
                  BOOK_PatientLastName:'JavaScript',
                  BOOK_PatientGender:'Male',
                  BOOK_PatientPostCode:'3046',
              },
            })
            .then(response=>{
                console.log("Response ",response);                  //UNID returned as a response

            }).catch(error=>console.log("Error",error));


        /**
         * Delete Document from the Database by using UNID
         */

        const DeleteDocument = await database.bulkDeleteDocumentsByUnid({
          unids: ['A793F287C3568117CA25848D001FDFDE',],
        })
        .then(response=>{
            console.log("Errors");
        })
        .catch(error=>{
          console.log("Error returned",error);
        });
    })
    .then(async database=>{
      //console.log(database.getTitle());
   })
   .catch(error=>{console.log("Error ",error)});
   //.catch(err=>{ console.log("Promise rejected")});

    //console.log("This is an array",typeof(crntarray[0]));
   console.log(crntarray[0])

   app.get('/',(req,res)=>{
    res.send(crntarray[2]);
    res.send('Hello World');
  });

  app.listen(9000,()=>{console.log('server live')});
  